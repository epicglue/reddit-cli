package main

import "gitlab.com/epicglue/reddit-cli/cmd"

func main() {
	cmd.Execute()
}
